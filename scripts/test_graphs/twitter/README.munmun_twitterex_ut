Twitter user–hashtag network, part of the Koblenz Network Collection
===========================================================================

This directory contains the TSV and related files of the munmun_twitterex_ut network:

This is a bipartite network consisting of Twitter users and tags they mentioned in their postings. Left nodes represent users and right nodes represent tags. An edge shows that a tag was used by a user in a tweet.


More information about the network is provided here: 
http://konect.uni-koblenz.de/networks/munmun_twitterex_ut

Files: 
    meta.munmun_twitterex_ut -- Metadata about the network 
    out.munmun_twitterex_ut -- The adjacency matrix of the network in space separated values format, with one edge per line
      The meaning of the columns in out.munmun_twitterex_ut are: 
        First column: ID of from node 
        Second column: ID of to node
        Third column: edge weight
        Fourth column: timestamp of the edge


Complete documentation about the file format can be found in the KONECT
handbook, in the section File Formats, available at:

http://konect.uni-koblenz.de/publications

All files are licensed under a Creative Commons Attribution-ShareAlike 2.0 Germany License.
For more information concerning license visit http://konect.uni-koblenz.de/license.



Use the following References for citation:

@MISC{konect:2016:munmun_twitterex_ut,
    title = {Twitter user–hashtag network dataset -- {KONECT}},
    month = oct,
    year = {2016},
    url = {http://konect.uni-koblenz.de/networks/munmun_twitterex_ut}
}

@inproceedings{konect:choudhury10,
        title = {How Does the Data Sampling Strategy Impact the
                  Discovery of Information Diffusion in Social Media?},
        author = {Choudhury, Munmun De and Yu-Ru Lin and Hari Sundaram and
                  Candan, K. Selçuk and Lexing Xie and Aisling
                  Kelliher}, 
        booktitle = {ICWSM}, 
        year = {2010}, 
        pages = {34--41}, 
}


@inproceedings{konect,
	title = {{KONECT} -- {The} {Koblenz} {Network} {Collection}},
	author = {Jérôme Kunegis},
	year = {2013},
	booktitle = {Proc. Int. Conf. on World Wide Web Companion},
	pages = {1343--1350},
	url = {http://userpages.uni-koblenz.de/~kunegis/paper/kunegis-koblenz-network-collection.pdf}, 
	url_presentation = {http://userpages.uni-koblenz.de/~kunegis/paper/kunegis-koblenz-network-collection.presentation.pdf},
}


