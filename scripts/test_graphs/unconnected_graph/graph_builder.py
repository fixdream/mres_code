# run this script with python {path}\graph_builder.py 10 10 (path)filename.txt
# requires pip install of  argparse (python pip install --user networkx)

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("butterflies", help="How many butterflies do you want in the graph", type=int)
parser.add_argument("path_and_file_name", help="Where do you want the file to go, and what should it be called", type=str)
args = parser.parse_args()


def write_graph_to_file(file, butterflies=0):
    f = open(file, "w+")
    if butterflies > 0:
        f.write("# This is an unconnected graph which has " + str(butterflies) + " butterflies \n")
    cur_a = 0
    cur_b = 1
    next_a = 2
    next_b = 3
    for x in range(args.butterflies):
        f.write(str(cur_a) + " " + str(cur_b) + "\n")
        f.write(str(cur_a) + " " + str(next_b) + "\n")
        f.write(str(next_a) + " " + str(cur_b) + "\n")
        f.write(str(next_a) + " " + str(next_b) + "\n")
        cur_a += 4
        cur_b += 4
        next_a += 4
        next_b += 4
    f.close()


write_graph_to_file(args.path_and_file_name, args.butterflies)

print("Your graph file has been created with " + str(args.butterflies) + " butterflies")
