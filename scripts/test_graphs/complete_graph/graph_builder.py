# This graph uses the python networkx library to save test graphs
# https://www.python-course.eu/networkx.php
# https://networkx.github.io/documentation/stable/reference/algorithms/bipartite.html

# run this script with python {path}\graph_builder.py 10 10 (path)filename.txt
# python C:\Users\Paddy\Dropbox\Uni\Masters\project\mres_code\scripts\testgraphs\complete_graph\graph_builder.py 10 10 "C:\Users\Paddy\Dropbox\Uni\Masters\project\mres_code\scripts\testgraphs\complete_graph\graph.txt"
# requires pip install of networkx, ittertools and argparse (python pip install --user networkx)

import networkx as nx
import argparse
from itertools import combinations

parser = argparse.ArgumentParser()
parser.add_argument("side_a", help="How many vertices do you want to have on the first side of the graph", type=int)
parser.add_argument("side_b", help="How many vertices do you want to have on the second side of the graph", type=int)
parser.add_argument("path_and_file_name", help="Where do you want the file to go, and what should it be called", type=str)
args = parser.parse_args()

# number of butterflies the graph will have
side_a_combinations = len(list(combinations(range(0, args.side_a), 2)))
side_b_combinations = len(list(combinations(range(0, args.side_b), 2)))
butterflies = side_a_combinations * side_b_combinations

# Complete Graph
g = nx.complete_bipartite_graph(args.side_a, args.side_b)

def write_graph_to_file(edges, file, butterflies = 0):
    f = open(file, "w+")
    if butterflies > 0:
        f.write("# This is a complete bipartite graph which is " + str(args.side_a) + " x " + str(args.side_b) + " with " + str(butterflies) + " butterflies \n")
    for key, value in edges():
        f.write(str(key) + " " + str(value) + "\n")
    f.close()

write_graph_to_file(g.edges, args.path_and_file_name, butterflies)

print("Your graph file has been created with " + str(butterflies) + " butterflies")
