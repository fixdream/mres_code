import argparse
from itertools import combinations
import time
start_time = time.time()


parser = argparse.ArgumentParser()
parser.add_argument("lines", help="How many lines do you want to see", type=int)
parser.add_argument("path_and_file_name", help="Where do you want the file to go, and what should it be called", type=str)
args = parser.parse_args()



with open(args.path_and_file_name) as input_file:
    line = input_file.readline()
    count = 0
    lines = ""
    while count <= args.lines:
        lines += line
        line = input_file.readline()
        count += 1

print(lines)