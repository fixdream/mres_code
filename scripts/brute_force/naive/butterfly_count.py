import argparse
from itertools import combinations
import time
start_time = time.time()


parser = argparse.ArgumentParser()
parser.add_argument("path_and_file_name", help="Where do you want the file to go, and what should it be called", type=str)
args = parser.parse_args()
graph = {}


def load_graph(graph_file):
    with open(graph_file) as input_file:
        line = input_file.readline()

        while line:
            split_line = line.split()

            # ignore # commented lines
            if split_line[0] == "#":
                line = input_file.readline()
                continue

            # ignore # commented lines
            if split_line[0] == "%":
                line = input_file.readline()
                continue

            if split_line[0] in graph.keys():
                graph[split_line[0]].add(split_line[1])
            else:
                graph[split_line[0]] = {split_line[1]}
            line = input_file.readline()
    return graph

def count_butterflies(graph):
    position = 0
    butterflies = 0
    graph_length = len(graph)
    for vs in graph.values():
        # skip the last v as its already been tested
        if graph_length == (position + 1):
            break

        test_pos = 0
        v_count = len(vs)
        for test in graph.values():

            # skip the test as this has already been tested (or is the same line)
            if position >= test_pos:
                test_pos += 1
                continue
            # test for butterflies
            total_vs = v_count + len(test)
            union = vs.copy()
            union.update(test)
            s =total_vs - len(union)
            if s > 1:
                butterflies += len(list(combinations(range(0, s), 2)))
            test_pos += 1

        position += 1
    return int(butterflies)

graph = load_graph(args.path_and_file_name)
butterflies = count_butterflies(graph)
print("There were " + str(butterflies) + " in the graph")
print("--- Execution took %s seconds ---" % (time.time() - start_time))