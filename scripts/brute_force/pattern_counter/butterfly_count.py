import argparse
from itertools import combinations
import sys
import collections
import time

start_time = time.time()
parser = argparse.ArgumentParser()
parser.add_argument("path_and_file_name", help="Where do you want the file to go, and what should it be called", type=str)
args = parser.parse_args()


def load_graph_to_memory(graph_file):
    # load data from a file to the memory - expects files to be an edge on each line
    with open(graph_file) as input_file:
        line = input_file.readline()
        while line:
            line_list = line.split(" ")
            split_line = [line_list[0], line_list[1]]
            # ignore # commented lines
            if split_line[0] == "#":
                line = input_file.readline()
                continue
            if split_line[0] == "%":
                line = input_file.readline()
                continue
            vs_count = (len(split_line) - 1)
            pos = 1
            while pos <= vs_count:
                left = str(split_line[0])
                right = str(split_line[pos])
                right = right.replace('\n', '')
                if left in graph:
                    graph[str(left)].append(int(right))
                else:
                    graph[str(left)] = [int(right)]
                # here we are actually doubling the size of the graph as the input file only represents the relationships
                # in one direction but we need to do this to represent the data structure after they are assembled in order of degree and
                # the right neighbours are removed
                if right in graph:
                    graph[str(right)].append(int(left))
                else:
                    graph[str(right)] = [int(left)]
                pos += 1
            line = input_file.readline()
    return graph


def order_vertices(d, order="degree"):
    # order the vertices by some criteria - degree high to low by default
    sorted_graph = {}
    if order == "degree":
        for k in sorted(d, key=lambda k: len(d[k]), reverse=True):
            sorted_graph[k] = d[k]
    else:
        print("The order specified " + order + " was invalid")
        sys.exit()
    return sorted_graph


def remove_right_neighbours(graph):
    # we want to remove all the vertices in each set that are to the right of the current vertex
    # so if you take this set as and example:
    # {'6': [0, 1, 2, 3], '7': [0, 1, 2, 3], '1': [4, 5, 6, 7], '2': [4, 5, 6, 7], '3': [4, 5, 6, 7], '4': [1, 2, 3], '5': [1, 2, 3], '0': [6, 7]}
    # in dictionary value '7' it lists 3 and '3' is to the right so can will need to go
    # this process will remove half of the vertices, which is ideal as we have each edge represented in both directions from the way we loaded the graph
    count = 0
    new_graph = {}
    right_neighbourhood = list(graph.keys())
    for key, value in graph.items():
        vertices = []
        for i in range(len(value)):
            if not (str(value[i]) in right_neighbourhood):
                vertices.append(value[i])
        new_graph[str(key)] = vertices
        right_neighbourhood.pop(int(0))
        count += 1
    return new_graph


def pattern_one_and_two_count(graph, ordered_vs):
    count = 0
    for X, UVs in graph.items():
        # So I define X as the right hand vertex in the pattern and W as its counterpart in the pattern on the same side as X
        # Then U and V are the shared vertices that both W and X have edges to

        # If there is less than 2 vertices X has edges with then there cannot be a U and a V so we skip this X candidate
        if len(UVs) < 2:
            continue

        # Now we get all of the combinations of U and V as the U and V candidates
        # (combinations rather than permutations as we do not care which is U and which is V)
        u_and_v_candidates = list(combinations(UVs, 2))

        # now we iterate through the U and V candidates and make the intersection of their edges which are the W candidates
        for c in u_and_v_candidates:
            uorv = str(c[0])
            voru = str(c[1])
            union = graph[uorv] + graph[voru]
            w_candidates = [item for item, count in collections.Counter(union).items() if count > 1]
            if len(w_candidates) >= 1:
                # We now cycle trough W candidates and see if they conform to the pattern
                for w in w_candidates:
                    w_candidate = str(w)
                    # Here is where the particular pattern we are looking for becomes relevant, the ordering on the vertices and removal of right neighbours
                    # already made all the U and V candidates valid, now we only care where the W is situated in our ordered graph in relation to U and V

                    # get the relative position of the W candidate to the U or V candidates
                    right_of_uorv = right_or_left(w_candidate, uorv, ordered_vs)
                    right_of_voru = right_or_left(w_candidate, voru, ordered_vs)

                    # pattern 1 (W U V X) we need to see W is further left than both U and V
                    if right_of_uorv == 0 and right_of_voru == 0:
                        count += 1

                    # pattern 2 (U W V X) we need to see W is left of one UV candidate and right of the other UV candidate
                    if (right_of_uorv == 1 and right_of_voru == 0) or (right_of_uorv == 0 and right_of_voru == 1):
                        count += 1

    print("pattern 1 and 2 count = " + str(count))
    return count


def right_or_left(subject, comparison, order):
    for v in order:
        if str(v) == str(subject):
            # the subject is left of the comparison
            return 0
        if str(v) == str(comparison):
            # the subject is right of the comparison
            return 1
    print("right_or_left function - something went wrong neither value was in the $order list given")


def pattern_three_count(graph):
    c = {}
    for v, edges in graph.items():
        if len(edges) > 1:
            edge_combinations = list(combinations(edges, 2))
            for v_edges in edge_combinations:
                key = {}
                key[0] = v_edges[0] if int(v_edges[0]) > int(v_edges[1]) else v_edges[1]
                key[1] = v_edges[1] if int(v_edges[0]) > int(v_edges[1]) else v_edges[0]
                hash = str(key[0]) + "-" + str(key[1])
                if hash not in c.keys():
                    c[hash] = 1
                else:
                    c[hash] += 1
    return int(sum(k*(k-1)/2 for k in c.values()))


# get data ready
graph = {}
graph = load_graph_to_memory(args.path_and_file_name)
graph = order_vertices(graph, "degree")
vs_order = list(graph.keys())
graph = remove_right_neighbours(graph)

# now we have the data we need, lets count the patterns
butterflies = 0
butterflies += pattern_one_and_two_count(graph, vs_order)  # pattern 1 (a and b) and pattern 2 (a and b)
butterflies += pattern_three_count(graph)  # pattern 3 (a and b)

print("Count complete, the number of butterflies is: " + str(butterflies))
print("--- Execution took %s seconds ---" % (time.time() - start_time))

