import operator as op
from functools import reduce

class Test:

    def __init__(self):
        # original input
        self.dict = {}
        self.butterfly_dict = {}
        self.butterflies = 0

    def count_butterflies(self, both_sides = True):
        # get data ready
        self.create_dictionary("pattern_graph.txt")
        common = 0
        for key, value in self.butterfly_dict.items():
            common += value
        print(len(self.butterfly_dict))
        self.butterflies += self.ncr(len(self.butterfly_dict), 2)
        if both_sides:
            self.butterflies = self.butterflies
        print("There are "+str(self.butterflies)+" butterflies in this graph.")

    def ncr(self, n, r):
        # https://stackoverflow.com/questions/4941753/is-there-a-math-ncr-function-in-python
        r = min(r, n - r)
        numer = reduce(op.mul, range(n, n - r, -1), 1)
        denom = reduce(op.mul, range(1, r + 1), 1)
        return int(numer / denom)

    def create_dictionary(self, file):
        # load data from a file to the memory - expects files to be an edge on each line
        with open("../files/input/" + file) as input_file:
            line = input_file.readline()
            while line:
                split_line = line.split()
                if split_line[1] in self.dict:
                    self.dict[split_line[1]] += 1
                else:
                    self.dict[split_line[1]] = 1
                line = input_file.readline()

        # create dictionary with only counts greater than 2
        for key, value in self.dict.items():
            if value > 1:
                self.butterfly_dict[key] = value
        print(self.dict)
        print(self.butterfly_dict)
