import json
import os


class Format:
    def __init__(self):
        # load the config file and set the required attributes
        config = json.load(open('..\config.json'))

        machine = config["machine"][0]
        self.os = machine["os"]

        files = config["files"][0]
        self.original_file_location = self.correct_slashes(files["original_file_location"])
        self.original_file_format = files["original_file_format"]
        self.processed_file_folder = self.correct_slashes(files["processed_file_folder"])
        self.max_file_lines = files["max_file_lines"]
        self.input_raw = ""

        # create/wipe output file
        self.file_name = os.path.basename(self.original_file_location)
        self.output_path = self.processed_file_folder + self.file_name
        self.create_or_wipe_output_file()

    def format(self):
        # check that the input file exists
        if not os.path.isfile(self.original_file_location):
            print("The input file did not load")
            return 0

        # for multi edge line files
        if self.original_file_format == "multi_edge_lines":
            return self.format_multi_edge()

        # for single edge line files
        if self.original_file_format == "single_edge_lines":
            return self.format_single_edge()

    def format_multi_edge(self):
        with open(self.original_file_location) as input_file:
            line = input_file.readline()
            count = 1
            output = ""
            while line:
                # remove #/* commented lines
                first_char = line[:1]
                if first_char == "#" or first_char == "*":
                    line = input_file.readline()
                    continue

                split_line = line.split()
                node = split_line[0]
                first_edge = split_line[1]

                # add the first edge line
                output += node + " " + first_edge + "\n"

                # any more edges?
                split_line.remove(node)
                split_line.remove(first_edge)
                for x in split_line:
                    if x != "" and x != " " and x != "  ":
                        # add extra line
                        output += node + " " + x + "\n"

                line = input_file.readline()
                count += 1

                if count >= self.max_file_lines:
                    break

        with open(self.output_path, "a") as out_file:
            out_file.write(output)

        return 1

    def format_single_edge(self):
        # with open(self.original_file_location) as input_file:
        #     line = input_file.readline()
        #     count = 1
        #     output = ""
        #     while line:
        #         # remove #/* commented lines
        #         first_char = line[:1]
        #         if first_char == "#" or first_char == "*":
        #             line = input_file.readline()
        #             continue
        #
        #         split_line = line.split()
        #         node = split_line[0]
        #         first_edge = split_line[1]
        #
        #         # add the first edge line
        #         output += node + " " + first_edge + "\n"
        #
        #         # any more edges?
        #         split_line.remove(node)
        #         split_line.remove(first_edge)
        #         for x in split_line:
        #             if x != "" and x != " " and x != "  ":
        #                 # add extra line
        #                 output += node + " " + x + "\n"
        #
        #         line = input_file.readline()
        #         count += 1
        #
        #         if count >= self.max_file_lines:
        #             break
        #
        # with open(self.output_path, "a") as out_file:
        #     out_file.write(output)

        return 1

    def create_or_wipe_output_file(self):
        open(self.output_path, 'w+').close()

    def correct_slashes(self, path):
        if self.os == "windows":
            return path.replace("/", '\\')
        else:
            return path.replace("\\", '/')


